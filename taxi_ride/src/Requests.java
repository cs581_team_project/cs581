import java.sql.Timestamp;

public class Requests {
    private int id;
    private Timestamp tpep_pickup_datetime;
    private Timestamp tpep_dropoff_datetime;
    private int passenger_count;
    private double trip_distance;
    private int PULocationID;
    private int DOLocationID;
    private int socialScore;

    public int getSocialScore() {
        return socialScore;
    }

    public void setSocialScore(int socialScore) {
        this.socialScore = socialScore;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getTpep_pickup_datetime() {
        return tpep_pickup_datetime;
    }

    public void setTpep_pickup_datetime(Timestamp tpep_pickup_datetime) {
        this.tpep_pickup_datetime = tpep_pickup_datetime;
    }

    public Timestamp getTpep_dropoff_datetime() {
        return tpep_dropoff_datetime;
    }

    public void setTpep_dropoff_datetime(Timestamp tpep_dropoff_datetime) {
        this.tpep_dropoff_datetime = tpep_dropoff_datetime;
    }

    public int getPassenger_count() {
        return passenger_count;
    }

    public void setPassenger_count(int passenger_count) {
        this.passenger_count = passenger_count;
    }

    public double getTrip_distance() {
        return trip_distance;
    }

    public void setTrip_distance(double trip_distance) {
        this.trip_distance = trip_distance;
    }

    public int getPULocationID() {
        return PULocationID;
    }

    public void setPULocationID(int pULocationID) {
        PULocationID = pULocationID;
    }

    public int getDOLocationID() {
        return DOLocationID;
    }

    public void setDOLocationID(int dOLocationID) {
        DOLocationID = dOLocationID;
    }

    public Requests(int id, Timestamp tpep_pickup_datetime, Timestamp tpep_dropoff_datetime, int passenger_count,
            double trip_distance, int pULocationID, int dOLocationID, int socialScore) {
        this.id = id;
        this.tpep_pickup_datetime = tpep_pickup_datetime;
        this.tpep_dropoff_datetime = tpep_dropoff_datetime;
        this.passenger_count = passenger_count;
        this.trip_distance = trip_distance;
        this.PULocationID = pULocationID;
        this.DOLocationID = dOLocationID;
        this.socialScore = socialScore;

    }
}
