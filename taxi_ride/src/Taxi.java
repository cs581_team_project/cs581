import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Taxi {
    private int taxi_id;
    private List<Requests> requests;
    private int start_location;
    private Timestamp startTime;
    private int end_location;
    private int current_passenger_count=0;
    private Double total_distance=0.0;
    private Double actual_distance = 0.0;
    private static int taxi_id_counter=0;
    private int socialScore;
    private List<Distances> route;

    // public int getTaxi

    public Taxi(List<Requests> requests, int start_location, int end_location,Timestamp startTime,int current_passenger_count, int socialScore) {
        this.taxi_id = ++taxi_id_counter;
        this.requests = requests;
        this.start_location = start_location;
        this.end_location = end_location;
        this.startTime=startTime;
        this.current_passenger_count=current_passenger_count;
        this.route = new ArrayList<>();
        this.socialScore = socialScore;
        // this.total_distance = requests.get(0).getTrip_distance();
    }

    public int getTaxi_id() {
        return taxi_id;
    }

    public void setTaxi_id(int taxi_id) {
        this.taxi_id = taxi_id;
    }

    public List<Requests> getRequests() {
        return requests;
    }

    public void setRequests(List<Requests> requests) {
        this.requests = requests;
    }

    public int getCurrent_passenger_count() {
        return current_passenger_count;
    }

    public void setCurrent_passenger_count(int current_passenger_count) {
        this.current_passenger_count = current_passenger_count;
    }

    public int getStart_location() {
        return start_location;
    }

    public void setStart_location(int start_location) {
        this.start_location = start_location;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Double getTotal_distance() {
        return total_distance;
    }

    public void setTotal_distance(Double total_distance) {
        this.total_distance = total_distance;
    }

    public Double getActual_distance() {
        return actual_distance;
    }

    public void setActual_distance(Double actual_distance) {
        this.actual_distance = actual_distance;
    }

    public List<Distances> getRoute() {
        return route;
    }

    public void setRoute(List<Distances> route) {
        this.route = route;
    }

    public int getEnd_location() {
        return end_location;
    }

    public void setEnd_location(int end_location) {
        this.end_location = end_location;
    }

    public int getSocialScore() {
        return socialScore;
    }

    public void setSocialScore(int socialScore) {
        this.socialScore = socialScore;
    }

    
}
