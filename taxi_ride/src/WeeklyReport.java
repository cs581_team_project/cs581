public class WeeklyReport {
    public int WeekNumber;
    private String WeekStartDate;
    private String WeekEndDate;
    private Double WeekDayDistanceBeforeMerge;
    private Double WeekDayDistanceAfterMerge;
    private Double WeekEndDistanceBeforeMerge;
    private Double WeekEndDistanceAfterMerge;
    private Double WeekDayDistanceSaving;
    private Double WeekEndDistanceSaving;


    public WeeklyReport(int weekNumber, String weekStartDate, String weekEndDate, Double weekDayDistanceBeforeMerge, Double weekDayDistanceAfterMerge, Double weekEndDistanceBeforeMerge, Double weekEndDistanceAfterMerge) {
        WeekNumber = weekNumber;
        WeekStartDate = weekStartDate;
        WeekEndDate = weekEndDate;
        WeekDayDistanceBeforeMerge = weekDayDistanceBeforeMerge;
        WeekDayDistanceAfterMerge = weekDayDistanceAfterMerge;
        WeekEndDistanceBeforeMerge = weekEndDistanceBeforeMerge;
        WeekEndDistanceAfterMerge = weekEndDistanceAfterMerge;
        WeekDayDistanceSaving = weekDayDistanceBeforeMerge - weekDayDistanceAfterMerge;
        WeekEndDistanceSaving = weekEndDistanceBeforeMerge - weekEndDistanceAfterMerge;

    }

    public int getWeekNumber() {
        return WeekNumber;
    }

    public String getWeekStartDate() {
        return WeekStartDate;
    }

    public String getWeekEndDate() {
        return WeekEndDate;
    }

    public Double getWeekDayDistanceBeforeMerge() {
        return WeekDayDistanceBeforeMerge;
    }

    public Double getWeekDayDistanceAfterMerge() {
        return WeekDayDistanceAfterMerge;
    }

    public Double getWeekEndDistanceBeforeMerge() {
        return WeekEndDistanceBeforeMerge;
    }

    public Double getWeekEndDistanceAfterMerge() {
        return WeekEndDistanceAfterMerge;
    }

    public Double getWeekDayDistanceSaving() {
        return WeekDayDistanceSaving;
    }

    public Double getWeekEndDistanceSaving() {
        return WeekEndDistanceSaving;
    }
}
