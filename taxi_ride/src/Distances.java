public class Distances {
    private int id;
    private int location_id1;
    private int location_id2;
    private double distance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLocation_id1() {
        return location_id1;
    }

    public void setLocation_id1(int location_id1) {
        this.location_id1 = location_id1;
    }

    public int getLocation_id2() {
        return location_id2;
    }

    public void setLocation_id2(int location_id2) {
        this.location_id2 = location_id2;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Distances(int id, int location_id1, int location_id2, double distance) {
        this.id = id;
        this.location_id1 = location_id1;
        this.location_id2 = location_id2;
        this.distance = distance;
    }

    
}
