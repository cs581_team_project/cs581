
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit ;
import java.util.stream.Collectors;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

public class App {

    static Connection con;
    ArrayList<Taxi> taxiList;
    ArrayList<Taxi> taxitripFinished;
    List<Requests> requestList;
    int requestsInADay=0,mergeTripsInADay=0;
    ArrayList<Distances> distances;
    double totalDistancesForAllRequests=0;
    double actualTotalDistanceAfterMerging=0;
    double totalDistanceInADay=0.0,distanceAfterMergingInADay=0.0;
    static int wt;
    static final int MIN_IN_MILLISECONDS=60*1000;
    static final float MILES_IN_METERS= (float) 1609.34;
    boolean forADay=false,forAMonth=false;


    public static void main(String[] args) throws Exception {
        App app = new App();
        Scanner sc = new Scanner(System.in);
        con = getConnection();
        app.loadDistances();
        System.out.println("Please select one of the following actions:");
        System.out.println("1 -- Merge Trips for a day");
        System.out.println("2 -- Merge Trips for a day where pickup location is LaGuardia airport");
        System.out.println("3 -- Merge Trips for a day where drop off location is LaGuardia airport");
        System.out.println("4 -- Merge Trips for whole month of September 2019");
        System.out.println("5 -- Analyze distance saving for different waiting time");

        int action = Integer.parseInt(sc.next());
        //I am taking data only for a particular date
        String d = "";
        if(action ==1 || action == 2 || action == 3 || action ==5){
            System.out.println("Enter the Date : [FORMAT - DD/MM/YYYY]");//MM/dd/YYYY
             d = sc.next();
        }
        if(action!=5) {
            System.out.println("Enter the waiting time");
            wt = sc.nextInt();
        }
        // starting time
        long start = System.nanoTime();
        sc.close();
        switch (action)
        {
            case 1:
                app.forADay=true;
                app.mergeTripForADayWithSamePickup(d);
                app.mergeTripForADayWithSameDropOff(d);
                break;
            case 2:
                app.mergeTripForADayWithSamePickup(d);
                break;
            case 3:
                app.mergeTripForADayWithSameDropOff(d);
                break;
            case 4:
                app.forAMonth=true;
                app.analyzeWeekdayAndWeekendSaving();  //took 10 min to execute
                break;
            case 5:
                app.analyzeSavingForDifferentWaitingTime(d);
                break;
            default:
                break;
        }

        // ending time 
        long end = System.nanoTime();
        System.out.println("Time taken to run the algorithm " + (double)(end - start) / 1_000_000_000.0 + " s");
        
        
        con.close();
    }

    private void loadDistances(){
        Distances d;
        distances = new ArrayList<>();
        //Fetch complete distance matrix
        String sql = "select * from Distances;";
        Statement stmt;
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                d= new Distances(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getDouble(4));
                distances.add(d);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void insertMergeData() {
        Statement st;
        try {
            st = con.createStatement();
            String sql="insert into merge_trip_info(total_distance_before_merge,total_distance_after_merge,distance_saved) values ("+
                totalDistanceInADay+","+distanceAfterMergingInADay+","+(
                totalDistanceInADay-distanceAfterMergingInADay)+");";
            st.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }

    
    private void allocateTaxiForSameDropOff(Requests value) {
        Taxi t;
        List<Requests> requests;
        boolean taxiAllocated=false;
        if(taxiList.size()==0){
            requests = new ArrayList<>();
            requests.add(value);
            t=new Taxi(requests, value.getPULocationID(), value.getDOLocationID(),value.getTpep_pickup_datetime() ,value.getPassenger_count(),value.getSocialScore());
            t.getRoute().add(distances.stream().filter(d->d.getLocation_id1()==value.getPULocationID() && 
            d.getLocation_id2()== value.getPULocationID()).collect(Collectors.toList()).get(0));
            taxiList.add(t);
            taxiAllocated=true;
        }else{
            List<Integer> feasiblePULocations;
            Iterator<Taxi> itr=taxiList.iterator();
            while(itr.hasNext() && !taxiAllocated){
                Taxi taxi=itr.next();
                if(taxi.getStartTime().getTime()+(MIN_IN_MILLISECONDS*wt)>value.getTpep_pickup_datetime().getTime()){
                    if(taxi.getCurrent_passenger_count()+value.getPassenger_count()<=3 &&
                    taxi.getSocialScore()==value.getSocialScore()){
                        feasiblePULocations=distances.stream()
                        .filter(d-> d.getLocation_id1()==taxi.getStart_location() && d.getDistance()<5*MILES_IN_METERS )
                        .map(Distances::getLocation_id2)
                        .collect(Collectors.toList());
                        if(feasiblePULocations.contains(value.getPULocationID())){
                            taxi.setCurrent_passenger_count(taxi.getCurrent_passenger_count()+value.getPassenger_count());
                            List<Requests> requestsList=taxi.getRequests();
                             requestsList.add(value);
                            taxi.setRequests(requestsList);
                            taxiAllocated=true;
                            if(taxi.getCurrent_passenger_count()==3){
                                taxitripFinished.add(taxi);
                                itr.remove();
                            }
                        }
                    }
                }else{
                    //the trip has started
                    taxitripFinished.add(taxi);
                    itr.remove();
                }
            }
            if(!taxiAllocated){
                requests=new ArrayList<>();
                requests.add(value);
                t=new Taxi(requests, value.getPULocationID(), value.getDOLocationID(), value.getTpep_pickup_datetime(),value.getPassenger_count(),value.getSocialScore());
                t.getRoute().add(distances.stream().filter(d->d.getLocation_id1()==value.getPULocationID() && 
                    d.getLocation_id2()==value.getPULocationID()).collect(Collectors.toList()).get(0));
                taxiList.add(t);
            }
        }

    }

    private void allocateTaxiForSamePickup(Requests value) {
        Taxi t;
        List<Requests> requests;
        boolean taxiAllocated=false;
        if(taxiList.size()==0){
            requests = new ArrayList<>();
            requests.add(value);
            t=new Taxi(requests, value.getPULocationID(), value.getDOLocationID(),value.getTpep_pickup_datetime() ,value.getPassenger_count(),value.getSocialScore());
            t.getRoute().add(distances.stream().filter(d->d.getLocation_id1()==value.getPULocationID() && 
            d.getLocation_id2()== value.getPULocationID()).collect(Collectors.toList()).get(0));
            taxiList.add(t);
            taxiAllocated=true;
        }else{
            List<Integer> feasibleDOLocations;
            Iterator<Taxi> itr=taxiList.iterator();
            while(itr.hasNext() && !taxiAllocated){
                Taxi taxi=itr.next();
                if(taxi.getStartTime().getTime()+(MIN_IN_MILLISECONDS*wt)>value.getTpep_pickup_datetime().getTime()){
                    if(taxi.getCurrent_passenger_count()+value.getPassenger_count()<=3 &&
                    taxi.getSocialScore()==value.getSocialScore()){
                        feasibleDOLocations=distances.stream()
                        .filter(d-> d.getLocation_id1()==taxi.getEnd_location() && d.getDistance()<5*MILES_IN_METERS )
                        .map(Distances::getLocation_id2)
                        .collect(Collectors.toList());
                        if(feasibleDOLocations.contains(value.getDOLocationID())){
                            taxi.setCurrent_passenger_count(taxi.getCurrent_passenger_count()+value.getPassenger_count());
                            List<Requests> requestsList=taxi.getRequests();
                             requestsList.add(value);
                            taxi.setRequests(requestsList);
                            taxiAllocated=true;
                            if(taxi.getCurrent_passenger_count()==3){
                                taxitripFinished.add(taxi);
                                itr.remove();
                            }
                        }
                    }
                }else{
                    //the trip has started
                    taxitripFinished.add(taxi);
                    itr.remove();
                }
            }
            if(!taxiAllocated){
                requests=new ArrayList<>();
                requests.add(value);
                t=new Taxi(requests, value.getPULocationID(), value.getDOLocationID(), value.getTpep_pickup_datetime(),value.getPassenger_count(),value.getSocialScore());
                t.getRoute().add(distances.stream().filter(d->d.getLocation_id1()==value.getPULocationID() && 
                    d.getLocation_id2()==value.getPULocationID()).collect(Collectors.toList()).get(0));
                taxiList.add(t);
            }
        }

    }

    private void calculateDistances() {
        for(Taxi taxi:taxitripFinished){

            List<Distances> routelist = taxi.getRoute();
            for(Requests requests:taxi.getRequests()){
                taxi.setTotal_distance(taxi.getTotal_distance()+requests.getTrip_distance());
            }
            
            List<Distances> list=distances;
            List<Integer> PULocation = taxi.getRequests().stream().filter(r->r.getPULocationID()!=taxi.getStart_location())
                                    .map(Requests::getPULocationID).collect(Collectors.toList());
            while(PULocation.size()!=0){
                list=distances.stream().filter(d->d.getLocation_id1()==routelist.get(routelist.size()-1).getLocation_id2() &&
                PULocation.contains(d.getLocation_id2())).collect(Collectors.toList());
                Collections.sort(list,Comparator.comparing(Distances::getDistance));
                taxi.getRoute().add(list.get(0));
                PULocation.remove(Integer.valueOf(list.get(0).getLocation_id2()));
            }
            

            List<Integer> DoLocation = taxi.getRequests().stream().map(Requests::getDOLocationID).collect(Collectors.toList());
            /** Filtering dropoff locations that are closer to the last passenger as the first dropoff and so on */
            while(DoLocation.size()!=0){
                list=distances.stream()
                    .filter(d-> d.getLocation_id1()==routelist.get(routelist.size()-1).getLocation_id2() && 
                    DoLocation.contains(d.getLocation_id2()))
                    .collect(Collectors.toList()); 
                Collections.sort(list,Comparator.comparing(Distances::getDistance));
                taxi.getRoute().add(list.get(0));
                DoLocation.remove(Integer.valueOf(list.get(0).getLocation_id2()));
                
            }

            for (Distances d : taxi.getRoute()) {
                taxi.setActual_distance(taxi.getActual_distance() + d.getDistance() / MILES_IN_METERS);
            }

            totalDistancesForAllRequests += taxi.getTotal_distance();
            actualTotalDistanceAfterMerging += taxi.getActual_distance();
            
        }
    }


    private void analyzeSavingForDifferentWaitingTime(String d){
        int[] waitingTimes = { 5, 7, 10, 15};  //min
        double[] fromSavings = new double[6];
        double[] toSavings = new double[6];
        // forADay=true;

        int i=0;
        for (int waitingTime : waitingTimes) {
            wt= waitingTime;
            System.out.println("\n For Waiting Time : "+wt+"\n\n");
            mergeTripForADayWithSamePickup(d);
            fromSavings[i] = totalDistancesForAllRequests - actualTotalDistanceAfterMerging;
            totalDistancesForAllRequests =0;
            actualTotalDistanceAfterMerging=0;

            mergeTripForADayWithSameDropOff(d);
            toSavings[i] = totalDistancesForAllRequests - actualTotalDistanceAfterMerging;
            totalDistancesForAllRequests =0;
            actualTotalDistanceAfterMerging=0;
            i++;

        }
        System.out.println("========================================Reports=================================================\n");
        for(int j=0; j<waitingTimes.length; j++){
            System.out.println("Trips from LaGuardia airport: Waiting Time = " + waitingTimes[j]+ " min, Distance Saving = "+ fromSavings[j]+" miles");
            System.out.println("Trips to LaGuardia airport: Waiting Time = " + waitingTimes[j]+ " min, Distance Saving = "+ toSavings[j]+" miles");
        }

    }

    private  void analyzeWeekdayAndWeekendSaving() throws ParseException {
        List<WeeklyReport> reports = new ArrayList<WeeklyReport>();
        int week = 1;
        LocalDate startDate = LocalDate.of(2019,7,1);
        while(week <=4){
            //weekday
            double weekdayDistanceBefore = 0.0;
            double weekendDistanceBefore = 0.0;
            double weekdayDistanceAfter = 0.0;
            double weekendDistanceAfter = 0.0;
            for(int i=0; i<5;i++){
                this.totalDistanceInADay=0.0;
                this.distanceAfterMergingInADay=0.0;
                totalDistancesForAllRequests =0;
                actualTotalDistanceAfterMerging=0;
                String date = startDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                mergeTripForADayWithSamePickup(date);
                totalDistancesForAllRequests =0;
                actualTotalDistanceAfterMerging=0;
                mergeTripForADayWithSameDropOff(date);
                weekdayDistanceBefore+= this.totalDistanceInADay;
                weekdayDistanceAfter+=this.distanceAfterMergingInADay;
                startDate = startDate.plusDays(1);
            }
            for(int i=0;i<2;i++){
                this.distanceAfterMergingInADay=0.0;
                this.totalDistanceInADay=0.0;
                totalDistancesForAllRequests =0;
                actualTotalDistanceAfterMerging=0;
                String date = startDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                mergeTripForADayWithSamePickup(date);
                totalDistancesForAllRequests =0;
                actualTotalDistanceAfterMerging=0;
                mergeTripForADayWithSameDropOff(date);
                weekendDistanceBefore+= this.totalDistanceInADay;
                weekendDistanceAfter+=this.distanceAfterMergingInADay;
                startDate = startDate.plusDays(1);
            }
            reports.add(new WeeklyReport(week,startDate.minusDays(7).toString(),startDate.minusDays(1).toString(),weekdayDistanceBefore,weekdayDistanceAfter,weekendDistanceBefore,weekendDistanceAfter));
            week++;
        }
        writeToCSV(reports);

    }


    private void writeToCSV(List<WeeklyReport> weeklyReports)
    {
        final String CSV_LOCATION = "Python-Mysql Connector/reports.csv ";
        ICsvBeanWriter beanWriter = null;
        CellProcessor[] processors = new CellProcessor[] {
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull(),
                new NotNull()
        };

        try {
            beanWriter = new CsvBeanWriter(new FileWriter(CSV_LOCATION),
                    CsvPreference.STANDARD_PREFERENCE);
            String[] header = { "WeekNumber", "WeekStartDate", "WeekEndDate", "WeekDayDistanceBeforeMerge", "WeekDayDistanceAfterMerge", "WeekEndDistanceBeforeMerge","WeekEndDistanceAfterMerge","WeekDayDistanceSaving","WeekEndDistanceSaving" };

            beanWriter.writeHeader(header);
            System.out.println(weeklyReports.size());

            for (WeeklyReport report : weeklyReports) {
                beanWriter.write(report, header, processors);
            }

        } catch (IOException ex) {
            System.err.println("Error writing the CSV file: " + ex);
        } finally {
            if (beanWriter != null) {
                try {
                    beanWriter.close();
                } catch (IOException ex) {
                    System.err.println("Error closing the writer: " + ex);
                }
            }
        }
    }
    
    void mergeTripForADayWithSamePickup(String d){
        try {
            //Does SQL fetch and stores in array lists
            loadDataForSamePickup(d);
            for(Requests r: requestList){
                allocateTaxiForSamePickup(r);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        taxitripFinished.addAll(taxiList);
        if(!forADay && !forAMonth){
            System.out.println("Taxi Requests : " +requestList.size());
            System.out.println("Merged Trips: " +taxitripFinished.size());
        }else{
            requestsInADay+=requestList.size();
            mergeTripsInADay+=taxitripFinished.size();
        }

        calculateDistances();
        if(!forADay && !forAMonth){
            System.out.println("=========================================================================================================\n");
            System.out.println("Total distance covered for all the requests of the day: "+totalDistancesForAllRequests+" miles");
            System.out.println("Total distance covered for all the trips after merge algorithm : "+actualTotalDistanceAfterMerging+" miles");
            System.out.println("Total distance saved: "+(totalDistancesForAllRequests-actualTotalDistanceAfterMerging)+" miles");
            System.out.println("Total distance saved in %: " +((totalDistancesForAllRequests-actualTotalDistanceAfterMerging)/totalDistancesForAllRequests)*100+" %" );
            System.out.println("=========================================================================================================\n");
            System.out.println("Actual Carbon footprint: "+ totalDistancesForAllRequests*0.411+" kilograms");
            System.out.println("Carbon footprint after merge algorithm: "+ actualTotalDistanceAfterMerging*0.411+" kilograms");
            System.out.println("Carbon footprint reduced: "+(totalDistancesForAllRequests-actualTotalDistanceAfterMerging)*0.411+" kilograms");
            System.out.println("=========================================================================================================\n");

            insertMergeDataForSamePickup(d);
        }else{
            totalDistanceInADay+=totalDistancesForAllRequests;
            distanceAfterMergingInADay+=actualTotalDistanceAfterMerging;
        }
        

    }

    void mergeTripForADayWithSameDropOff(String d){
        try {
            //Does SQL fetch and stores in array lists
            loadDataForSameDropOff(d);
            for(Requests r: requestList){
                allocateTaxiForSameDropOff(r);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        taxitripFinished.addAll(taxiList);
        if(!forADay && !forAMonth){
            System.out.println("Taxi Requests : " +requestList.size());
            System.out.println("Merged Trips: " +taxitripFinished.size());
        }else{
            requestsInADay+=requestList.size();
            mergeTripsInADay+=taxitripFinished.size();
            if(!forAMonth){
                System.out.println("Taxi Requests : " +requestsInADay);
                System.out.println("Merged Trips: " +mergeTripsInADay);
            }
        }

        calculateDistances();
        if(!forADay && !forAMonth){
            System.out.println("=========================================================================================================\n");
            System.out.println("Total distance covered for all the requests of the day: "+totalDistancesForAllRequests+" miles");
            System.out.println("Total distance covered for all the trips after merge algorithm : "+actualTotalDistanceAfterMerging+" miles");
            System.out.println("Total distance saved: "+(totalDistancesForAllRequests-actualTotalDistanceAfterMerging)+" miles");
            System.out.println("Total distance saved in %: " +((totalDistancesForAllRequests-actualTotalDistanceAfterMerging)/totalDistancesForAllRequests)*100+" %" );
            System.out.println("=========================================================================================================\n");
            System.out.println("Actual Carbon footprint: "+ totalDistancesForAllRequests*0.411+" kilograms");
            System.out.println("Carbon footprint after merge algorithm: "+ actualTotalDistanceAfterMerging*0.411+" kilograms");
            System.out.println("Carbon footprint reduced: "+(totalDistancesForAllRequests-actualTotalDistanceAfterMerging)*0.411+" kilograms");
            System.out.println("=========================================================================================================\n");
            insertMergeDataForSameDropOff(d);
        }else{
            totalDistanceInADay+=totalDistancesForAllRequests;
            distanceAfterMergingInADay+=actualTotalDistanceAfterMerging;
            if(!forAMonth){
                System.out.println("Total Distance for all the requests of the day:"+totalDistanceInADay+" miles");
                System.out.println("Total Distance for all the trips after merging: "+distanceAfterMergingInADay+" miles");
                System.out.println("=========================================================================================================\n");
                System.out.println("Total distance saved: "+(totalDistanceInADay-distanceAfterMergingInADay)+" miles");
                System.out.println("Total distance saved in %: " +((totalDistanceInADay-distanceAfterMergingInADay)/totalDistanceInADay)*100+" %" );
                System.out.println("=========================================================================================================\n");
                System.out.println("Actual Carbon footprint: "+ totalDistanceInADay*0.411+" kilograms");
                System.out.println("Carbon footprint after merge algorithm: "+ distanceAfterMergingInADay*0.411+" kilograms");
                System.out.println("Carbon footprint reduced: "+(totalDistanceInADay-distanceAfterMergingInADay)*0.411+" kilograms");
                System.out.println("=========================================================================================================\n");
            
                insertMergeData();
            }
        }

    }
    private void insertMergeDataForSamePickup(String date) {
        Statement st;
        try {
            st = con.createStatement();
            Date d = (new SimpleDateFormat("dd/MM/yyyy")).parse(date);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String sqlDate = formatter.format(d);
            String sql= new StringBuilder().append("insert into merge_trip_pickup_laguardia(date,waiting_time,total_distance_before_merge,total_distance_after_merge,distance_saved) values (\'")
                    .append(sqlDate)
                    .append("\',")
                    .append(wt).append(",")
                    .append(totalDistancesForAllRequests).append(",")
                    .append(actualTotalDistanceAfterMerging).append(",")
                    .append(totalDistancesForAllRequests - actualTotalDistanceAfterMerging)
                    .append(");").toString();
            st.executeUpdate(sql);
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }

    }
    private void insertMergeDataForSameDropOff(String date) {
        Statement st;
        try {
            st = con.createStatement();
            Date d = (new SimpleDateFormat("dd/MM/yyyy")).parse(date);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String sqlDate = formatter.format(d);
            String sql= new StringBuilder().append("insert into merge_trip_dropoff_laguardia(date,waiting_time,total_distance_before_merge,total_distance_after_merge,distance_saved) values (\'")
                    .append(sqlDate)
                    .append("\',")
                    .append(wt).append(",")
                    .append(totalDistancesForAllRequests).append(",")
                    .append(actualTotalDistanceAfterMerging).append(",")
                    .append(totalDistancesForAllRequests - actualTotalDistanceAfterMerging)
                    .append(");").toString();
            st.executeUpdate(sql);
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }

    }
    private void loadDataForSamePickup(String date) throws Exception {
        Requests r;
        requestList = new ArrayList<>();
        taxiList = new ArrayList<>();
        taxitripFinished = new ArrayList<>();
        //Fetch complete distance matrix
        String sql;
        Statement stmt;
        ResultSet rs ;

        Date dtInitial  = new SimpleDateFormat("dd/MM/yyyy").parse(date);
        Date dtEnd = new Date(dtInitial.getTime()+ (1000 * 60 * 60 * 24));
        //Fetch requests for a particular day in ascending order
        sql = "Select * from taxi_trip where tpep_pickup_datetime >= '"+new Timestamp(dtInitial.getTime())
                +"' and tpep_pickup_datetime < '"+new Timestamp(dtEnd.getTime())+"' and PULocationID = 138 and passenger_count<3 order by tpep_pickup_datetime asc;";
        // System.out.println(sql);
        stmt = con.createStatement();
        rs = stmt.executeQuery(sql);
        while(rs.next()){
            r=new Requests(rs.getInt(1), rs.getTimestamp(2), rs.getTimestamp(3),rs.getInt(4), rs.getDouble(5), rs.getInt(6), rs.getInt(7),rs.getInt(9));
            requestList.add(r);
        }

    }

    private void loadDataForSameDropOff(String date) throws Exception {
        Requests r;
        requestList = new ArrayList<>();
        taxiList = new ArrayList<>();
        taxitripFinished = new ArrayList<>();
        //Fetch complete distance matrix
        String sql;
        Statement stmt;
        ResultSet rs;
        Date dtInitial  = new SimpleDateFormat("dd/MM/yyyy").parse(date);
        Date dtEnd = new Date(dtInitial.getTime()+ (1000 * 60 * 60 * 24));
        //Fetch requests for a particular day in ascending order
        sql = "Select * from taxi_trip where tpep_pickup_datetime >= '"+new Timestamp(dtInitial.getTime())
                +"' and tpep_pickup_datetime < '"+new Timestamp(dtEnd.getTime())+"' and DOLocationID = 138 and passenger_count<3 order by tpep_pickup_datetime asc;";
        // System.out.println(sql);
        stmt = con.createStatement();
        rs = stmt.executeQuery(sql);
        while(rs.next()){
            r=new Requests(rs.getInt(1), rs.getTimestamp(2), rs.getTimestamp(3),rs.getInt(4), rs.getDouble(5), rs.getInt(6), rs.getInt(7),rs.getInt(9));
            requestList.add(r);
        }

    }

    static Connection getConnection() {
        Connection con=null;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/taxi_ride","root","password");
        }catch(Exception e){
            e.printStackTrace();
        }
        return con;
    }
}
