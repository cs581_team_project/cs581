create table taxi_trip
(taxi_trip_ID int not null primary key auto_increment,
tpep_pickup_datetime datetime not null,
tpep_dropoff_datetime datetime not null,
passenger_count double not null,
trip_distance double not null,
PULocationID int not null,
DOLocationID int not null,
total_trip_time double);

describe taxi_trip;

use taxi_ride;

create table distances
(id int not null primary key auto_increment,
location_id1 int not null,
location_id2 int not null,
distance double not null);

describe distances;

set global local_infile=1;
show global variables like "local_infile";

LOAD DATA LOCAL INFILE '/Users/my/Desktop/CS581/export_dataframe.csv'  INTO TABLE taxi_ride.taxi_trip  FIELDS TERMINATED BY ','  ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 ROWS;

LOAD DATA LOCAL INFILE '/Users/my/Desktop/CS581/matrix.csv'  INTO TABLE taxi_ride.distances  FIELDS TERMINATED BY ','  ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 ROWS;

CREATE TABLE `merge_trip_info` ( `index` INT NOT NULL AUTO_INCREMENT, `total_distance_before_merge` FLOAT, `total_distance_after_merge` FLOAT, `distance_saved` FLOAT, PRIMARY KEY (`index`) );
CREATE TABLE `merge_trip_pickup_laguardia` ( `index` INT NOT NULL AUTO_INCREMENT, `date` datetime not null, `waiting_time` INT, `total_distance_before_merge` FLOAT, `total_distance_after_merge` FLOAT, `distance_saved` FLOAT, PRIMARY KEY (`index`) );
CREATE TABLE `merge_trip_dropoff_laguardia` ( `index` INT NOT NULL AUTO_INCREMENT, `date` datetime not null, `waiting_time` INT,  `total_distance_before_merge` FLOAT, `total_distance_after_merge` FLOAT, `distance_saved` FLOAT, PRIMARY KEY (`index`) );

delete from taxi_trip where (PULocationID = 138 and DOLocationID = 138);

alter table taxi_trip add social_score int;
update taxi_trip set social_score = round(rand()*5);